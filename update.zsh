#!/usr/bin/env zsh
zsh update-arch.zsh
zsh update-rpm.zsh

# the debian script will perform validation and extraction
zsh update-ubuntu.zsh download

zsh update-debian.zsh
zsh update-hashes.zsh
