#!/usr/bin/env zsh

echo BuildID
for file in **/libc*.so; do
    build_id=$(readelf -n $file 2>&1 | grep "Build ID" | cut -f 2 -d ':')

    if [[ -z "$build_id" ]];
    then
        continue
    fi

    link=hashes/build_id/${build_id//[[:blank:]]/}
    if [[ ! -z "$link" ]] && [[ ! -e "$link" ]]; then
        ln -sr $file $link
    fi
done &

echo SHA1
for file in **/libc*.so; do
    link=hashes/sha1/$(sha1sum $file | cut -f 1 -d ' ')
    if [[ ! -z "$link" ]] && [[ ! -e "$link" ]]; then
        ln -sr $file $link
    fi
done &

echo SHA256
for file in **/libc*.so; do
    link=hashes/sha256/$(sha256sum $file | cut -f 1 -d ' ')
    if [[ ! -z "$link" ]] && [[ ! -e "$link" ]]; then
        ln -sr $file $link
    fi
done &

echo MD5
for file in **/libc*.so; do
    link=hashes/md5/$(md5sum $file | cut -f 1 -d ' ')
    if [[ ! -z "$link" ]] && [[ ! -e "$link" ]]; then
        ln -sr $file $link
    fi
done &

wait
wait
wait
wait

git add hashes
git commit -m 'Update hashes'