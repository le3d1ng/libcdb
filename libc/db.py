import sys, re

expr = r'GNU C Library( \S+)+'

for filename in sys.argv[1:]:
    with open(filename, 'rb') as f:
        data = f.read()
        if data[:4] != '\x7fELF':
            continue
        if not re.search(expr, data):
            print sys.argv[1]
