#!/usr/bin/env zsh

function is_marked {
    # If it's a deb-file, don't try to read it
    if file "$1" | grep XZ > /dev/null;
    then
        false
        return
    fi

    # Ensure the commit actually exists
    commit="$(head -c40 $1)"
    if [ "$(git cat-file -t $commit 2>/dev/null)" = "commit" ];
    then
        true
        return
    fi

    false
    return
}

function mark {
    rm -f $1
    echo $2 > $1
}


function check {
    echo Implement me...
}

function download() {
    URLS=(
    "http://seblu.net/a/arm/packages/g/glibc/"
    "http://seblu.net/a/arm/packages/l/lib32-glibc/"
    )


    for URL in $URLS;
    do
        wget \
         --follow-ftp \
         --no-parent \
         --execute robots=off \
         --no-proxy \
         --level=1 \
         --no-parent \
         --recursive \
         --no-clobber \
         --no-directories \
         --accept "*libc*.xz" \
         --reject "*-bin*" \
         --reject "*-dbg*" \
         --reject "*-doc*" \
         --reject "*-prof*" \
         --reject "*-udeb*" \
         --reject "*-xen*" \
         --reject "*-source*" \
         --reject "*-pic*" \
         --reject "*linux-libc-dev*" \
         --verbose \
         --directory-prefix $1 \
         $URL &
    done

    for URL in $URLS;
    do
        wait
    done
}



function extract {
for deb in $1/**/*.xz(.);
do
    echo $deb
    dir="libc/${deb:t:r}"

    echo "Checking $deb"
    if is_marked "$deb" ;
    then
        echo "...skipping"
        continue
    fi

    [[ -d $dir ]] || mkdir -p $dir

    echo "Extracting $deb"
    tar \
        --directory "$dir" \
        --wildcards \
        --wildcards-match-slash \
        --extract \
        --file "$deb" \
        '*libc.so.*' \
        '*libc-*.so*' \
        '*libc.a' \
        '*libc.so*'

    echo "Committing $deb"
    git add libc || continue
    git commit -m "$deb"

    mark $deb $(git rev-parse HEAD)
    git add $deb
    git commit -m "update .xz"
done
}

case "$1" in
    check)
        check    archfiles
        ;;
    download)
        download archfiles
        ;;
    extract)
        extract  archfiles
        ;;
    *)
        check    archfiles
        download archfiles
        extract  archfiles
        ;;
esac
