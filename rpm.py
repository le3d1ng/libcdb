#!/usr/bin/env python
import requests
import multiprocessing
import os
import time
import urllib
from HTMLParser import HTMLParser
from urlparse import urljoin

URLs = [
# "http://rpmfind.net/linux/rpm2html/search.php?query=libc.so.6",
]

FedoraArchive = [
"https://dl.fedoraproject.org/pub/archive/fedora/linux/",
"https://archives.fedoraproject.org/pub/fedora/linux/"
]

FedoraIndices = [
    "updates/%(release)s/%(arch)s/",
    "releases/%(release)s/Everything/%(arch)s/os/Packages/%(prefix)s",
    "releases/%(release)s/Everything/%(arch)s/os/Packages/"
]

FedoraArches = ['armhfp',
                'i386',
                'x86_64',
                'ppc',
                'ppc64']
FedoraArches.extend([i + '.newkey' for i in FedoraArches])

# glibc, dietlibc
PackagePrefixes = ["g","d"]

for archive in FedoraArchive:
    for index in FedoraIndices:
        for release in range(7,25):
            for arch in FedoraArches:
                for prefix in PackagePrefixes:
                    url = archive + index
                    URLs.append(url % locals())

blacklist = [
'-bin',
'-doc',
'-dbg',
'-debug',
'-prof',
'-source',
'-src',
'-xen',
'udeb',
'linux-libc-dev',
'-pic',
'-utils',
'-header',
'-common',
'-devel',
'msp430',
'avr',
'delta'
]

whitelist = [
'/glibc',
'dietlibc',
'-libc-',
'-glibc-',
]


Q_rpm = multiprocessing.Queue()
Q_dir = multiprocessing.Queue()

def curl(url):
    try:
        r = requests.get(url, timeout=5)
    except Exception:
        print 'Error: %s' % (url)
        return None

    print '%s: %s' % (r.status_code, url)
    if r.ok: return r.content

class WgetMirror(HTMLParser):
    def __init__(self, url):
        HTMLParser.__init__(self)
        self.url = url

    def handle_starttag(self, tag, attrs):
        if tag != 'a': return

        attrs = dict(attrs)
        href = attrs.get('href', None)
        if not href:
            return

        if not href.endswith('rpm'):
            return

        href = href.replace('ftp://', 'http://')

        if '://' not in href:
            href = urljoin(self.url, href)

        Q_rpm.put(href)

def rpm_worker(Q):
    for url in iter(Q.get, "STOP"):
        path = os.path.basename(url)

        if any(B in url for B in blacklist):
            # print "Black: ", path
            continue

        if not any(W in url for W in whitelist):
            # print "!White:", path
            continue

        if os.path.exists(path):
            print "Skip: ", path
            continue


        with open(path, 'wb+') as f:
            data = curl(url)
            if not data:
                continue
            f.write(data)

def dir_worker(Q):
    for url in iter(Q.get, "STOP"):
        html = curl(url)
        if html:
            WgetMirror(url).feed(html)

os.chdir('rpmfiles')

workers = []
queues  = {
dir_worker: Q_dir,
rpm_worker: Q_rpm
}

for function, queue in queues.items():
    for w in xrange(20):
        p = multiprocessing.Process(target=function, args=(queue,))
        p.start()
        workers.append(p)

for URL in URLs:
    Q_dir.put(URL)

while not Q_dir.empty(): time.sleep(1)
while not Q_rpm.empty(): time.sleep(1)

for w in workers:
    w.terminate()